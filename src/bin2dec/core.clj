(ns bin2dec.core
  (:gen-class :main true))

(defn expt
  "Return x to the power of n, where n is a non-negative integer."
  [x n]
  (cond
    (< n 0) (expt (/ 1 x) (* n -1))
    (zero? n) 1
    (zero? x) 0
    (= (mod n 2) 0) (expt (* x x) (/ n 2))
    (= (mod n 2) 1) (* x (expt (* x x) (/ (- n 1) 2)))))

(defn get-user-input
  "Prompt the user and return their input."
  [prompt]
  (print prompt)
  (flush)
  (read-line))

(defn bin2dec
  "Convert a binary string to a decimal number."
  [binary-number]
  (let [formatted-binary-number (clojure.string/reverse
                                (clojure.string/join ""
                                                     (clojure.string/split binary-number #"[^0-1]")))]
    (loop [remaining-digits formatted-binary-number
          i 0
          decimal 0]
      (if (empty? remaining-digits)
        decimal
        (let [[digit & remaining] remaining-digits]
          (recur remaining
                (inc i)
                (+ decimal (* (Integer. (str digit)) (expt 2 i)))))))))

(defn -main
  "The main function."
  []
  (let [binary-number (get-user-input "Enter your input: ")]
    (println (bin2dec binary-number))))
