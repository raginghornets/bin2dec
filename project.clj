(defproject bin2dec "0.1.0-SNAPSHOT"
  :description "Binary to decimal converter"
  :url "https://github.com/airicbear/bin2dec"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.10.0"]]
  :repl-options {:init-ns bin2dec.core}
  :main bin2dec.core)
